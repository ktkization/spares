var deployd = require('deployd');

var server_port = process.env.OPENSHIFT_NODEJS_PORT || 8080;
var server_ip_address = process.env.OPENSHIFT_NODEJS_IP || '127.0.0.1';

var server = deployd({
  port: process.env.PORT || 5000,
  env: 'production',
  db: {
    host: '127.8.55.130',
    port: '27017',
    name: 'spares',
    credentials: {
      username: 'admin',
      password: 'LHTHelIymVgJ'
    }
  }
});

server.listen(server_port, server_ip_address);

server.on('listening', function() {
  console.log("Server is listening");
});

server.on('error', function(err) {
  console.error(err);
  process.nextTick(function() { // Give the server a chance to return an error
    process.exit();
  });
});
