app.factory('Login', function($resource){
  return $resource('/users/login');
});

app.factory('Logout', function($resource){
  return $resource('/users/logout');
});

app.factory('Retailer', function($resource){
  return $resource('/users/me');
});

app.factory('Users', function($resource){
  return $resource('/users/:id', {id: '@id'});
});

app.factory('Products', function($resource){
  return $resource('/products/:id', {id: '@id'});
});

app.factory('Orders', function($resource){
  return $resource('/orders/:id', {id:'@id'});
});
