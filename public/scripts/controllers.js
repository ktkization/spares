var updId='';

app.controller('AccountCtrl', function($scope, $rootScope, $location, $timeout, Login, Logout, Retailer, Users) {
  $scope.error = '';

  Retailer.get(function(res) {
    if (res.hasOwnProperty('id')) {
      $location.path('/dash');
    }
  });


  $scope.signup = function(user, pass, rt_pass, name, email, phone, loc, addr) {
    if (pass !== rt_pass) {
      $scope.error = 'Password Mismatch. Please Retype!';
      $scope.pass = '';
      $scope.rt_pass = '';
    } else {
      var data = {
        username: user,
        password: pass,
        retailer: name,
        email: email,
        phone: phone,
        location: loc,
        address: addr
      }
      var resp = Users.save(data, function(res) {
        alert("Successfuly Created Account!");
        $location.path('/');
      });

      $timeout(function() {
        if (!resp.hasOwnProperty('id')) {
          $scope.error = 'Error! Such a user exists';
        }
      }, 1000);

    }
  }

  $scope.login = function(user, pass) {

    var data = {
      username: user,
      password: pass
    }

    var res = Login.save(data, function(res) {
      $location.path('/dash');
    });

    $timeout(function() {
      if (!res.hasOwnProperty('id')) {
        $scope.error = 'Login Failed! Wrong username or password';
      }
    }, 1000);

  }
});

app.controller('DashCtrl', function($scope, $rootScope, $mdDialog, $mdMedia, $location, $timeout,$route, Retailer, Products, Orders, Logout) {

  var user = null;

  $scope.products = [];

  $scope.orders= [];

  Retailer.get(function(res) {
    if (!res.hasOwnProperty('id')) {
      $location.path('/');
    }

    user = res;

    $scope.accountDetails = {
      Username: user.username,
      Retailer: user.retailer,
      Email: user.email,
      Phone: '0' + user.phone,
      Location: user.location,
      Address: user.address
    }

    $scope.shop = user.retailer;
    $scope.userid = user.id;

    var prods = Products.query(function(res) {
      res.forEach(function(item) {
        console.log(item);
        $scope.products.push(item);
      });
    });

    var ords= Orders.query({retailerId: $scope.userid}, function(res) {
      res.forEach(function(item) {
        console.log(item);
        $scope.orders.push(item);
      });
    })
  });

  $scope.logout = function() {
    Logout.save(function() {
      $location.path('/');
    });
  }

  $scope.addNewDialog = function() {
    $mdDialog.show({
      controller: ProductDialogCtrl,
      templateUrl: './views/products.tmpl.html',
      clickOutsideToClose: true
    });
  }

  $scope.changePasswordDialog = function() {
    $mdDialog.show({
      controller: ChangePasswordCtrl,
      templateUrl: './views/change_password.tmpl.html',
      clickOutsideToClose: true
    });
  }

  $scope.changeAccountDialog = function() {
    $mdDialog.show({
      controller: ChangeAccountCtrl,
      templateUrl: './views/change_account.tmpl.html',
      clickOutsideToClose: true
    });
  }

  $scope.delProduct = function(id) {
    console.log(id);
    Products.delete({id:id})
    $route.reload();
  }

  $scope.updateProductDialog = function(id) {
    console.log(id);
    updId= id;
    $mdDialog.show({
      controller: UpdateProductCtrl,
      templateUrl: './views/update_product.tmpl.html',
      clickOutsideToClose: true
    });
  }

  $scope.makeSale= function(id){

    Orders.save({id:id, status: 'completed'});
    $route.reload();

  }

  $scope.cancelSale= function(id){
    Orders.save({id:id, status: 'cancelled'});
    $route.reload();
  }
});

function UpdateProductCtrl($scope, $mdDialog, Products, Retailer, $route) {
  Products.get({id: updId}, function(res) {

    $scope.typ = res.type;
    $scope.make = res.make;
    $scope.model = res.model;
    $scope.year = res.year;
    $scope.quantity = res.quantity;
    $scope.price= res.price;

  });

  $scope.updateProduct = function() {
    Retailer.get(function(res) {
      console.log(res);
      var ret = res;
      var data = {
        type: $scope.typ,
        make: $scope.make,
        model: $scope.model,
        year: $scope.year,
        rating: 0,
        retailerid: ret.id,
        location: ret.location,
        quantity: $scope.quantity,
        price: $scope.price,
      }

      Products.save({id:updId}, data);

      $mdDialog.hide();
      $route.reload();
    });
  }
}

function ChangeAccountCtrl($scope, $mdDialog, Retailer, Users, $route) {
  $scope.id = null;

  Retailer.get(function(res) {
    user = res;

    $scope.name = user.retailer;
    $scope.email = user.email;
    $scope.phone = parseInt(('0' + user.phone));
    $scope.loc = user.location;
    $scope.addr = user.address;

    $scope.id = user.id;

  });

  $scope.changeAcc = function(name, email, phone, loc, addr) {
    var data = {
      id: $scope.id,
      retailer: name,
      email: email,
      phone: phone,
      location: loc,
      address: addr
    }
    var resp = Users.save(data, function(res) {
      alert("Successfuly changed your Details!");
      $route.reload();
    });

    $mdDialog.hide();

  }
}

function ChangePasswordCtrl($scope, $mdDialog, Retailer, Users) {
  $scope.id = null;

  Retailer.get(function(res) {
    user = res;

    $scope.id = user.id;

    $scope.pass = '';
    $scope.rt_pass = '';

  });

  $scope.changePass = function(pass, rt_pass) {
    if (pass !== rt_pass) {
      $scope.error = 'Password Mismatch. Please Retype!';
      $scope.pass = '';
      $scope.rt_pass = '';
    } else {
      var data = {
        id: $scope.id,
        password: pass,
      }
      var resp = Users.save(data, function(res) {
        alert("Successfuly Changed password!");
        $location.path('/dash');
      });

      $mdDialog.hide();

    }
  }
}

function ProductDialogCtrl($scope, $mdDialog, $route, Products, Retailer) {
  $scope.addNew = function() {
    Retailer.get(function(res) {
      console.log(res);
      var ret = res;
      var data = {
        type: $scope.typ,
        make: $scope.make,
        model: $scope.model,
        year: $scope.year,
        rating: 0,
        retailerid: ret.id,
        location: ret.location,
        quantity: $scope.quantity,
        price: $scope.price,
      }

      Products.save(data);

      $mdDialog.hide();
      $route.reload();
    });
  }
}
