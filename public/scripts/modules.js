app= angular.module('Spares', ['ngRoute','ngMaterial', 'ngMessages', 'ngResource',]);

app.config(function($routeProvider, $mdThemingProvider){
  $mdThemingProvider.theme('default')
    .primaryPalette('teal', {
      'default': '900',
      'hue-1': '800',
      'hue-2': '700',
      'hue-3': '500'
    })
    .accentPalette('amber', {
      'default': '900',
      'hue-1': '800',
      'hue-2': '600',
      'hue-3': '500'
    });

  $routeProvider.
  when('/',{
    templateUrl: './views/login.html',
    controller: 'AccountCtrl'
  }).
  when('/signup',{
    templateUrl: './views/signup.html',
    controller: 'AccountCtrl'
  }).
  when('/dash',{
    templateUrl: './views/dash.html',
    controller: 'DashCtrl'
  }).
  when('/search',{
    templateUrl: './views/mobile/search.html',
    controller: 'SearchCtrl'
  }).
  when('/results',{
    templateUrl: './views/mobile/results.html',
    controller: 'SearchResultsCtrl'
  }).
  when('/results-cat',{
    templateUrl: './views/mobile/results_cat.html',
    controller: 'SearchResultsCtrl'
  }).
  when('/view-item',{
    templateUrl: './views/mobile/view_item.html',
    controller: 'SearchResultsCtrl'
  }).
  when('/checkout',{
    templateUrl: './views/mobile/checkout.html',
    controller: 'CheckoutCtrl'
  }).
  otherwise({
    redirectTo:'/'
  });
});
