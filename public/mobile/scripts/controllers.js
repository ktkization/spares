app.controller('AppCtrl', function($scope, _, $resource, $location, Users, Products, Orders) {
  $scope.products = {
    makes: null,
    models: null,
    years: null,
    locations: null
  };

  $scope.searchData = {
    makes: [],
    models: [],
    years: [],
    locations: []
  };

  Products.query(function(res) {
    _.each(res, function(item) {
      if (!_.contains($scope.searchData.makes, item.make)) {
        $scope.searchData.makes.push(item.make);
      }
      if (!_.contains($scope.searchData.models, item.model)) {
        $scope.searchData.models.push(item.model);
      }
      if (!_.contains($scope.searchData.years, item.year)) {
        $scope.searchData.years.push(item.year);
      }
      if (!_.contains($scope.searchData.locations, item.location)) {
        $scope.searchData.locations.push(item.location);
      }
    });

    $scope.products.makes = getIndexedObjects($scope.searchData.makes);
    $scope.products.models = getIndexedObjects($scope.searchData.models);
    $scope.products.years = getIndexedObjects($scope.searchData.years);
    $scope.products.locations = getIndexedObjects($scope.searchData.locations);

    console.log($scope.products);

  });

  function getIndexedObjects(arr) {
    var objs = {};
    if (_.size(arr) < 1) {
      return null;
    } else {
      var len = _.size(arr);
      var ids = [];
      for (var i = 1; i <= len; i++) {
        objs[i] = arr[i - 1];
      }
      return objs;
    }
  }

  $scope.viewListing = function(p_make, p_model, p_year, p_location) {
    var queryData = {};

    if (p_make !== undefined) {
      queryData.make = p_make;
    }
    if (p_model !== undefined) {
      queryData.model= p_model;
    }
    if (p_year !== undefined) {
      queryData.year= p_year;
    }
    if (p_location !== undefined) {
      queryData.location= p_location;
    }

    Products.query(queryData, function(res) {
      $location.path('/results');

      $scope.prodResults= res;

      var tally={};

      _.each(res, function(obj) {
        tally[obj.type]= [];
      });

      _.each(res, function(obj) {
        if(tally.hasOwnProperty(obj.type)){
          tally[obj.type].push(1);
        }
      });

      _.each(res, function(obj) {
        tally[obj.type]= tally[obj.type].length;
      });

      $scope.searchResults= tally;
    });
  };

  $scope.viewCategory= function(item){
    $location.path('/results-cat');
    var catItems=[];

    _.each($scope.prodResults, function(obj) {
      if(obj.type===item){
        catItems.push(obj);
        console.log(obj);
      }
    });
    console.log(catItems);
    $scope.items= catItems;
  };

  $scope.upvote= function(id) {
    Products.get({id:id}, function(res) {
      var rt= res.rating;
      Products.save({id:id, rating: rt+1 });
    });
  };

  $scope.makeOrder= function(id, price, retId) {

    Products.get({id:id}, function(res) {
      $scope.cart= res.id;
    });

    $scope.itemPrice= parseInt(price);
    $scope.retId= retId;

    $location.path('/checkout');
  };

  $scope.checkout= function(name, phone, email, quantity){
    var data= {
      name: name,
      phone: phone,
      email: email,
      item: $scope.cart,
      quantity: quantity,
      cost: parseInt(($scope.itemPrice*quantity)),
      status: 'pending',
      retailerid: $scope.retId
    };

    console.log(data);

    Orders.save(data);

    $location.path('/finito');
  };
});
