app = angular.module('Spares', ['ngRoute', 'ngResource']);

app.constant('_', window._);

app.config(function($routeProvider){
  $routeProvider.
  when('/',{
    templateUrl: './views/search.html'
  }).
  when('/results',{
    templateUrl: './views/results.html'
  }).
  when('/results-cat',{
    templateUrl: './views/results_cat.html'
  }).
  when('/checkout',{
    templateUrl: './views/checkout.html'
  }).
  when('/finito',{
    templateUrl: './views/finito.html'
  }).
  otherwise({
    redirectTo:'/'
  });
});
